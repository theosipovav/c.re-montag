document.addEventListener("DOMContentLoaded", function () {
	var tabMenu = document.querySelector(".catalog__content__menu"),
		tabMenuOffset = tabMenu.getBoundingClientRect().top;


	if (window.innerWidth > 767) {
		window.addEventListener('scroll', function () {
			if (window.scrollY > tabMenuOffset) {
				tabMenu.classList.add("is-fixed");
				document.querySelector(".catalog__tabs").classList.add("is-fixed");
			} else {
				tabMenu.classList.remove("is-fixed");
				document.querySelector(".catalog__tabs").classList.remove("is-fixed");
			}
		})
	}
})
