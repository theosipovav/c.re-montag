<?php
    require_once('php-scripts/user_functions.php');

    $page_title = 'РЕ-МОНТАЖ – Панель управления администратора';

    require_once('header.php');
    require_once('top_menu.php');
?>

<div class="col-xs-12 col-sm-8 col-md-9">
    <div class="inner_body">

        <?php

        if (isAdmin()) {

        echo '<div class="row"><div class="col-sm-3 col-sm-offset-9"><button type="submit" id="logout_btn" class="btn btn-success btn-block btn-sm">Выход</button></div></div>';

        echo '<h1>Панель управления администратора</h1>';

        $brands = array();
        $brand_query = $db->query("SELECT * FROM brands ORDER BY id");
        while ($brand_row = $brand_query->fetch_assoc()) {
            $brands[] = $brand_row['name'];            
        }

        $series = array();
        $series_query = $db->query("SELECT * FROM series ORDER BY id");
        while ($series_row = $series_query->fetch_assoc()) {
            $series[] = $series_row['name'];            
        }

        ?>

        <!-- ====== -->
        <!-- Брэнды -->
        <!-- ====== -->
        <h2 class="text-center admin_header" data-toggle="collapse" data-target="#brand_hide">Брэнды</h2>
        <div id="brand_hide" class="collapse">
        <table class="table">
        <tr><th>ID</th><th>Брэнд</th><th>Описание</th></tr>

        <?php
        $brands_query = $db->query("SELECT * FROM brands ORDER BY id");
        while ($brand_row = $brands_query->fetch_assoc()) {

            echo '<tr id="'.$brand_row['id'].'"><td width="30">'.$brand_row['id'].'</td>
                  <td width="120"><input name="brand_name" value="'.$brand_row['name'].'" class="input-sm form-control"></td>
                  <td><textarea name="brand_description" class="input-sm form-control">'.$brand_row['description'].'</textarea>
                  <a name="save_brand_btn" class="btn btn-sm btn-success pull-right disabled" style="width: 100%;">Сохранить</a>
                  </td></tr>';
        } ?>

        </table></div>

        <!-- ===== -->
        <!-- Серии -->
        <!-- ===== -->
        <h2 class="text-center admin_header" data-toggle="collapse" data-target="#series_hide">Серии</h2>
        <div id="series_hide" class="collapse">
        <table class="table">
        <tr><th>ID</th><th>Серия</th><th>Краткое описание</th><th>Описание</th></tr>

        <?php
        $series_query = $db->query("SELECT * FROM series ORDER BY brand_id, id");
        while ($series_row = $series_query->fetch_assoc()) {

            $brand_query = $db->query("SELECT name FROM brands WHERE id = '$series_row[brand_id]'");
            $brand_row = $brand_query->fetch_assoc();

            echo '<tr id="'.$series_row['id'].'">';
            echo '<td width="30">'.$series_row['id'].'</td>';
            echo '<td width="130"><input name="series_name" value="'.$series_row['name'].'" class="input-sm form-control"><br>'.$brand_row['name'].'</td>';
            echo '<td><textarea name="series_short_desc" class="input-sm form-control">'.$series_row['short_description'].'</textarea>
                  <a name="save_series_btn" class="btn btn-sm btn-success pull-right disabled" style="width: 100%;">Сохранить</a>
                  </td>
                  <td><textarea name="series_desc" class="input-sm form-control">'.$series_row['description'].'</textarea></td></tr>';
        } ?>

        </table></div>

        <!-- ====== -->
        <!-- Модели -->
        <!-- ====== -->
        <h2 class="text-center admin_header" data-toggle="collapse" data-target="#model_hide">Модели</h2>
        <div id="model_hide" class="collapse">
        <table class="table">
        <tr><th>Модель</th><th>Площадь</th><th>Цена</th><th colspan="2">Рекомендуемый</th></tr>

        <?php
        $models_query = $db->query("SELECT * FROM models ORDER BY brand_id, series_id, id");
        while ($model_row = $models_query->fetch_assoc()) {

            $model_name = $model_row['name'].' ('.$brands[$model_row['brand_id']-1].', серия '.$series[$model_row['series_id']-1].'):';

            echo '<tr><td style="padding: 8px 0 0 0;" colspan="5">'.$model_name.'</td></tr>';

            echo '<tr id="'.$model_row['id'].'">';
            echo '<td width="280" style="border:none;"><input name="model_name" type="text" value="'.$model_row['name'].'" class="input-sm form-control"></td>';
            echo '<td style="border:none;" width="120"><input name="area" type="text" value="'.$model_row['area'].'" class="input-sm form-control"></td>';
            echo '<td style="border:none;"><input name="price" type="text" value="'.$model_row['price'].'" class="input-sm form-control"></td>';

            $id_rec = ($model_row['recommended'] == '1') ? '1' : '0';
            $name_rec = ($model_row['recommended'] == '1') ? 'Да' : 'Нет';
            echo '<td style="border:none;"><div class="btn-group">
                    <button id="'.$id_rec.'" name="recommended_btn" type="button" data-toggle="dropdown" class="btn btn-sm btn-default dropdown-toggle">'.$name_rec.' <span class="caret"></span></button>
                    <ul class="dropdown-menu recommended_list">
                        <li><a id="1">Да</a></li>
                        <li><a id="0">Нет</a></li>
                    </ul></div></td>';

            echo '<td style="border:none;" width="70"><a name="save_model_btn" class="btn btn-sm btn-success pull-right disabled" style="width: 100%;">Сохранить</a></td></tr>';
        }

        } else { ?>

            <h1>Панель управления администратора</h1>

            <div class="row"><div class="col-sm-4 col-sm-offset-4">
                <form role="form">
                    <div class="form-group has-feedback">
                        <label for="login">Логин: <span>*</span></label>
                        <input name="login" class="form-control" id="login" placeholder="Введите логин" required>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="password">Пароль: <span>*</span></label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Введите пароль" required>
                    </div>
                    <button type="submit" id="login_btn" class="btn btn-success btn-block">Войти</button>
                </form>
            </div></div>

        <?php } ?>

        </table></div>
    </div>
</div>

<?php require_once('footer.php'); ?>