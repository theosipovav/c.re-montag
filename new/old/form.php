<?php
if (isset($_POST)) {
	$name 						= htmlspecialchars($_POST["name"]);
	$phone 						= htmlspecialchars($_POST["phone"]);
	$email 						= htmlspecialchars($_POST["email"]);
	$utm_source 			= htmlspecialchars($_POST["utm_source"]);
	$utm_medium 			= htmlspecialchars($_POST["utm_medium"]);
	$utm_campaign 		= htmlspecialchars($_POST["utm_campaign"]);
	$utm_term 				= htmlspecialchars($_POST["utm_term"]);
	$utm_content 			= htmlspecialchars($_POST["utm_content"]);
	$json 						= array();

	function mime_header_encode($str, $data_charset, $send_charset) {
		if($data_charset != $send_charset)
		$str=iconv($data_charset,$send_charset.'//IGNORE',$str);
		return ('=?'.$send_charset.'?B?'.base64_encode($str).'?=');
	}

	class TEmail {
		public $from_email;
		public $from_name;
		public $to_email;
		public $to_name;
		public $subject;
		public $data_charset='UTF-8';
		public $send_charset='windows-1251';
		public $body='';
		public $type='text/html';

		function send(){
			$dc=$this->data_charset;
			$sc=$this->send_charset;
			$enc_to=mime_header_encode($this->to_name,$dc,$sc).' <'.$this->to_email.'>';
			$enc_subject=mime_header_encode($this->subject,$dc,$sc);
			$enc_from=mime_header_encode($this->from_name,$dc,$sc).' <'.$this->from_email.'>';
			$enc_body=$dc==$sc?$this->body:iconv($dc,$sc.'//IGNORE',$this->body);
			$headers='';
			$headers.="Mime-Version: 1.0\r\n";
			$headers.="Content-type: ".$this->type."; charset=".$sc."\r\n";
			$headers.="From: ".$enc_from."\r\n";
			$headers .= 'Bcc: sales@marketing-na100.ru' . "\r\n";
			$headers .= 'Сc: floor@re-montag.ru' . "\r\n";
			return mail($enc_to,$enc_subject,$enc_body,$headers);
		}
	}

	$emailgo= new TEmail;
	$emailgo->from_email = 'site@c.re-montag.ru';
	$emailgo->from_name  = 'Ре-монтаж';
	$emailgo->to_email   = 'director@re-montag.ru';
	$emailgo->to_name    = 'Ре-монтаж';
	$emailgo->subject    = 'Заявка с сайта Ре-монтаж';
	if ($name) {
		$emailgo->body     = "<b>Имя:</b> " . $name . "<br>";
	}
	if ($phone) {
		$emailgo->body   	.= "<b>Телефон:</b> " . $phone . "<br>";
	}
	if ($email) {
		$emailgo->body   	.= "<b>E-mail:</b> " . $email . "<br>";
	}

	if ($utm_source) {
		$emailgo->body 		.= "<b>Рекламная система:</b> " . $utm_source . "<br>";
	}
	if ($utm_medium) {
		$emailgo->body 		.= "<b>Тип трафика:</b> " . $utm_medium . "<br>";
	}
	if ($utm_campaign) {
		$emailgo->body 		.= "<b>Рекламная кампания:</b> " . $utm_campaign . "<br>";
	}
	if ($utm_term) {
		$emailgo->body 		.= "<b>Ключевое слово:</b> " . $utm_term . "<br>";
	}
	if ($utm_content) {
		$emailgo->body 		.= "<b>Тип объявления:</b> " . $utm_content . "<br>";
	}

	$emailgo->send();

	//$json['error'] = 0;

	//echo json_encode($json);

	header("location: ".$_SERVER['HTTP_REFERER']);
} else {
	echo 'GET LOST!';
}
?>