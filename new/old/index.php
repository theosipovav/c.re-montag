<?php 
    require_once('php-scripts/user_functions.php');

    $page_title = 'РЕ-МОНТАЖ – Главная';
    $left_menu = false;

    require_once('header.php');
    require_once('top_menu.php');
?>

<div class="col-xs-12 col-sm-8 col-md-9">
    <div class="brand_main">
        <h1>Приветствуем Вас на сайте нашей компании!</h1>
        <p>Продажа, монтаж, обслуживание кондиционеров - одно из ведущих направлений деятельности ООО «РЕ-МОНТАЖ», <a href="images/Daichi.pdf">официального дилера на территории РФ</a>. Мы предлагаем только сертифицированную продукцию, качество которой соответствует международным стандартам, а цена является приемлемой. Наши кондиционеры создают уютную атмосферу в Вашем доме, офисе, комфортный климат в Вашем производственном помещении. Для нас важно, чтобы Вы остались довольны соотношением «цена-качество» выбранной модели кондиционера, сервисом нашей компании.</p>

        <div class="row">
            <div class="col-xs-12 col-sm-4"><a href="brand.php?id=2" class="brand_card" style="background-image: url(images/brands/brand_2.jpg);"></a></div>
            <div class="col-xs-12 col-sm-4"><a href="brand.php?id=1" class="brand_card" style="background-image: url(images/brands/brand_1.jpg);"></a></div>
            <div class="col-xs-12 col-sm-4"><a href="brand.php?id=3" class="brand_card" style="background-image: url(images/brands/brand_3.jpg);"></a></div>
            <div class="col-xs-12 col-sm-4"><a href="brand.php?id=4" class="brand_card" style="background-image: url(images/brands/brand_4.jpg);"></a></div>
            <div class="col-xs-12 col-sm-4"><a href="brand.php?id=6" class="brand_card" style="background-image: url(images/brands/brand_6.jpg);"></a></div>
        </div>
    </div>
</div>

<?php require_once('footer.php'); ?>