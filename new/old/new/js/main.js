$(document).ready(function() {

    $('.bxslider_top').bxSlider({
        mode: 'horizontal',
        controls: true,
        auto: true,
        captions: true
    });

    $('.bxslider_recomend').bxSlider({
        mode: 'fade',
        controls: true,
        auto: true,
        captions: false,
        pager: false,
        adaptiveHeight: true
    });

    $('.bxslider_series').bxSlider({
        mode: 'horizontal',
        controls: true,
        auto: false,
        captions: true,
        adaptiveHeight: true
    });

    // $(window).resize(function(){    
    //     var slider = $('.bxslider_top').bxSlider();
    //     // $('.bxslider_top').destroySlider();
    //     // var slider = sliderInit();
    //     slider.reloadSlider();
    // });

    // $(".fancybox").fancybox();

    // $('#slider').chocoslider();

    // function setAttr(prmName,val){
    //     var res = '';
    //     var d = location.href.split("#")[0].split("?");  
    //     var base = d[0];
    //     var query = d[1];
    //     if(query) {
    //         var params = query.split("&");  
    //         for(var i = 0; i < params.length; i++) {  
    //             var keyval = params[i].split("=");  
    //             if(keyval[0] != prmName) {  
    //                 res += params[i] + '&';
    //             }
    //         }
    //     }
    //     res += prmName + '=' + val;
    //     window.location.href = base + '?' + res;
    //     return false;
    // }

	/* Всплывающие подсказки */ 
	/* ===================== */
	// $('[data-toggle="tooltip"]').tooltip();
});