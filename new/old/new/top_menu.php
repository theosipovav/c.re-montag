<nav role="navigation" class="navbar navbar-default navbar-main navbar-fixed-top">
    <div class="container">
        <div class="row"><div class="col-xs-12 col-lg-10 col-lg-offset-1"><div class="row">
            <div class="col-xs-12 col-sm-4 header_logo">
                <a href="index.php"><img class="main_logo" src="images/logo.png"></a>
            </div>

            <div class="col-xs-12 col-sm-3">
                <input type="text" class="form-control input-sm search_input" placeholder="Поиск">
            </div>
 
            <div class="col-xs-12 col-sm-5">
                <div class="phone_header">22-12-00<span>8 (8412) 65-63-00</span></div>
            </div>
        </div></div></div>
    </div>
</nav>

<div class="right_buttons">
    <button class="button_mail" data-toggle="modal" data-target="#emailModal"></button>
    <button class="button_phone" data-toggle="modal" data-target="#callbackModal"></button>
    <button class="button_adress" data-toggle="modal" data-target="#adressModal"></button>
</div>

<main class="index_main">
    <div class="container">
        <div class="row"><div class="col-xs-12 col-lg-10 col-lg-offset-1">
            <div class="row">
                <div class="col-xs-12">
                    <ul class="bxslider_top" >
                        <li><a href="index.php"><img class="banner_container" src="images/banner/1.jpg"></a></li>
                        <li><a href="brand.php?id=1"><img class="banner_container" src="images/banner/2.jpg"></a></li>
                        <li><a href="brand.php?id=2"><img class="banner_container" src="images/banner/3.jpg"></a></li>
                        <li><a href="brand.php?id=4"><img class="banner_container" src="images/banner/4.jpg"></a></li>
                        <li><a href="brand.php?id=5"><img class="banner_container" src="images/banner/5.jpg"></a></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3"><center>
                    <img class="left_panel" src="images/left_panel.png">

                    <p class="recomend_text">Рекомендуемые товары</p>
                    <div class="recomend">
                        <ul class="bxslider_recomend" >

                        <?php $models_query = $db->query("SELECT * FROM models WHERE recommended = '1'");
                        while ($models_row = $models_query->fetch_assoc()) {

                            $brand_query = $db->query("SELECT name FROM brands WHERE id = '$models_row[brand_id]'");
                            $brand_name = $brand_query->fetch_assoc()['name'];

                            echo '<li><div class="recomend_item">
                                    <a class="title" href="">'.$brand_name.' '.$models_row['name'].'</a>
                                    <img src="images/brands/series/'.$models_row['series_id'].'/1.jpg">
                                    <p><b>Цена:</b> '.number_format($models_row['price'], 0, '', ' ').' ₽</p>
                                    <a class="details" href="model.php?id='.$models_row['id'].'">Описание товара</a>
                                </div></li>';
                        } ?>

                        </ul>
                    </div>

                    <?php if (!isset($left_menu) or ($left_menu == true)) {
                        $brand_query = $db->query("SELECT * FROM brands");
                        if ($brand_query !== false) {
                            echo '<ul class="left_menu">';
                            while ($brand_row_menu = $brand_query->fetch_assoc()) {
                                if (!isset($brand) or ($brand != $brand_row_menu['id'])) {
                                    $isActive = 'class="left_menu_button"';
                                } else {
                                    $isActive = 'class="left_menu_button active"';
                                }
                                echo '<li><a href="brand.php?id='.$brand_row_menu['id'].'" '.$isActive.'>'.$brand_row_menu['name'].'</a></li>';
                            }
                            echo '</ul>';
                        }
                    } ?>

                </center></div>
