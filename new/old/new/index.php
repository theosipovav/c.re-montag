<?php 
    require_once('php-scripts/user_functions.php');

    $page_title = 'РЕ-МОНТАЖ – Главная';
    $left_menu = false;

    require_once('header.php');
    require_once('top_menu.php');
?>

<div class="col-xs-12 col-sm-8 col-md-9">
    <div class="brand_main">
        <h1>Приветствуем Вас на сайте нашей компании!</h1>
        <p>Продажа, монтаж, обслуживание кондиционеров - одно из ведущих направлений деятельности ООО «РЕ-МОНТАЖ». Мы предлагаем только сертифицированную продукцию, качество которой соответствует международным стандартам, а цена является приемлемой. Наши кондиционеры создают уютную атмосферу в Вашем доме, офисе, комфортный климат в Вашем производственном помещении. Для нас важно, чтобы Вы остались довольны соотношением «цена-качество» выбранной модели кондиционера, сервисом нашей компании.</p>

        <div class="row">
        <?php for ($i = 1; $i < 7; $i++) { 
            echo '<div class="col-xs-12 col-sm-4"><a href="brand.php?id='.$i.'" class="brand_card" style="background-image: url(images/brands/brand_'.$i.'.jpg);"></a></div>';
        } ?>
        </div>
    </div>
</div>

<?php require_once('footer.php'); ?>