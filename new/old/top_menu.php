<nav role="navigation" class="navbar navbar-default navbar-main navbar-fixed-top">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 header_logo">
                        <a href="index.php"><img class="main_logo" src="images/remontag-logo.png"></a>
                        <span>Продажа, установка <br> и обслуживание кондиционеров <br> в Пензе и области</span>
                    </div>

                    <?/*
                    <div class="col-xs-12 col-sm-3">
                        <input type="text" class="form-control input-sm search_input" placeholder="Поиск">
                    </div>
                    */?>

                    <div class="col-xs-12 col-sm-4">
                        <div class="phone_header">22-12-00 <span>Код города (8412)</span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>

<div class="right_buttons">
    <button class="button_mail" data-toggle="modal" data-target="#emailModal"></button>
    <button class="button_phone" data-toggle="modal" data-target="#callbackModal"></button>
    <button class="button_adress" data-toggle="modal" data-target="#adressModal"></button>
</div>

<main class="index_main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-lg-10 col-lg-offset-1">
                <div class="row">
                    <div class="col-xs-12">
                        <ul class="bxslider_top" >
                            <li><a href="index.php"><img class="banner_container" src="images/banner/1.jpg"></a></li>
                            <li><a href="brand.php?id=1"><img class="banner_container" src="images/banner/2.jpg"></a></li>
                            <li><a href="brand.php?id=2"><img class="banner_container" src="images/banner/3.jpg"></a></li>
                            <li><a href="brand.php?id=3"><img class="banner_container" src="images/banner/4.jpg"></a></li>
                            <li><a href="brand.php?id=4"><img class="banner_container" src="images/banner/5.jpg"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form">
                            <div class="form__block">
                                <div class="form__block-title">
                                    <p>Консультация по кондиционерам и расчет стоимости</p>
                                </div>
                                <form action="/form.php" method="post" onsubmit="alert('Ваша заявка успешно отправлена!')">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="input text">
                                                <input type="text" name="name" placeholder="Ваше имя">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="input text">
                                                <input type="text" name="phone" placeholder="+7 (___)-___-__-__" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="input submit">
                                                <input type="submit" value="Отправить заявку">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <div class="form__footer">
                                    <p>Нажимая кнопку “Отправить заявку Вы автоматически соглашаетесь с нашей <a href="#">политикой конфиденциальности</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <center>
                            <img class="left_panel" src="images/left_panel.png">
                            <p class="recomend_text">Рекомендуемые товары</p>
                            <div class="recomend">
                                <ul class="bxslider_recomend" >
                                    <?php $models_query = $db->query("SELECT * FROM models WHERE recommended = '1'");
                                    while ($models_row = $models_query->fetch_assoc()) {

                                        $brand_query = $db->query("SELECT name FROM brands WHERE id = '$models_row[brand_id]'");
                                        $brand_name = $brand_query->fetch_assoc()['name'];

                                        echo '<li><div class="recomend_item">
                                                <a class="title" href="model.php?id='.$models_row['id'].'">'.$brand_name.' '.$models_row['name'].'</a>
                                                <img src="images/brands/series/'.$models_row['series_id'].'/1.jpg">
                                                <p><b>Цена:</b> '.number_format($models_row['price'], 0, '', ' ').' ₽</p>
                                                <a class="details" href="model.php?id='.$models_row['id'].'">Описание товара</a>
                                            </div></li>';
                                    } ?>
                                </ul>
                            </div>
                            <?php if (!isset($left_menu) or ($left_menu == true)) {
                                $brand_query = $db->query("SELECT * FROM brands");
                                if ($brand_query !== false) {
                                    echo '<ul class="left_menu">';
                                    while ($brand_row_menu = $brand_query->fetch_assoc()) {
                                        if (!isset($brand) or ($brand != $brand_row_menu['id'])) {
                                            $isActive = 'class="left_menu_button"';
                                        } else {
                                            $isActive = 'class="left_menu_button active"';
                                        }
                                        echo '<li><a href="brand.php?id='.$brand_row_menu['id'].'" '.$isActive.'>'.$brand_row_menu['name'].'</a></li>';
                                    }
                                    echo '</ul>';
                                }
                            } ?>
                        </center>
                    </div>
