$(document).ready(function() {

    $('.bxslider_top').bxSlider({
        mode: 'horizontal',
        controls: true,
        auto: true,
        captions: true
    });

    $('.bxslider_recomend').bxSlider({
        mode: 'fade',
        controls: true,
        auto: true,
        captions: false,
        pager: false,
        adaptiveHeight: true
    });

    $('.bxslider_series').bxSlider({
        mode: 'horizontal',
        controls: true,
        auto: false,
        captions: true,
        adaptiveHeight: true
    });

    $('.search_input').keypress(function (e) {
        var key = e.which;
        if(key == 13) {
            window.location.href = 'search.php?search=' + $(e.target).val().replace(' ','+');
            return false;
        }
    });

    /* Включаем кнопки в админке при редактировании полей */
    /* И разрешаем ввод только цифр в цену                */
    /* ================================================== */
    $('input[name=model_name]').keyup(function() {
        $(this).closest('tr').find('a[name=save_model_btn]').removeClass('disabled');
    });

    $('input[name=area]').keyup(function() {
        $(this).closest('tr').find('a[name=save_model_btn]').removeClass('disabled');
    });

    $('input[name=price]').keyup(function() {
        $(this).closest('tr').find('a[name=save_model_btn]').removeClass('disabled');
    });

    $("ul.recommended_list").on("click", function(e) {
        $(this).closest('tr').find('button[name=recommended_btn]').attr('id', (e.target || e.srcElement).id);
        var btn_str = ((e.target || e.srcElement).id == '1') ? 'Да' : 'Нет';
        btn_str += ' <span class="caret"></span>';
        $(this).closest('tr').find('button[name=recommended_btn]').html(btn_str);
        $(this).closest('tr').find('a[name=save_model_btn]').removeClass('disabled');
    });

    $('input[name=price]').keydown(function(event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || 
            event.keyCode == 173 || event.keyCode == 189 || event.keyCode == 109 || (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        } else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
                return;
            }
        }
    });

    /* Кнопка "Сохранить модель" */
    /* ========================= */
    $('a[name=save_model_btn]').click(function(e) {

        var id = $(this).closest('tr').attr('id');
        var name = $(this).closest('tr').find('input[name=model_name]').val();
        var area = $(this).closest('tr').find('input[name=area]').val();
        var price = $(this).closest('tr').find('input[name=price]').val();
        var recommended = $(this).closest('tr').find('button[name=recommended_btn]').attr('id');

        $.ajax({
            type: "post",
            url: "php-scripts/user_functions.php",
            data: "action=update_model&id=" + id + '&name=' + name + '&area=' + area + '&price=' + price + '&recommended=' + recommended,
            success: function(html){
                if (parseInt(html) == 200) {
                    $(e.target).addClass('disabled');
                    alert('Изменения успешно сохранены!');
                } else {
                    alert(html);
                }
            }
        });

        return false;
    });

    /* Включаем кнопки в админке при редактировании полей */
    /* ================================================== */
    $('input[name=brand_name]').keyup(function() {
        $(this).closest('tr').find('a[name=save_brand_btn]').removeClass('disabled');
    });

    $('textarea[name=brand_description]').keyup(function() {
        $(this).closest('tr').find('a[name=save_brand_btn]').removeClass('disabled');
    });

    /* Кнопка "Сохранить брэнд" */
    /* ======================== */
    $('a[name=save_brand_btn]').click(function(e) {

        var id = $(this).closest('tr').attr('id');
        var name = $(this).closest('tr').find('input[name=brand_name]').val();
        var description = $(this).closest('tr').find('textarea[name=brand_description]').text();

        $.ajax({
            type: "post",
            url: "php-scripts/user_functions.php",
            data: "action=update_brand&id=" + id + '&name=' + name + '&description=' + description,
            success: function(html){
                if (parseInt(html) == 200) {
                    $(e.target).addClass('disabled');
                    alert('Изменения успешно сохранены!');
                } else {
                    alert(html);
                }
            }
        });

        return false;
    });

    /* Включаем кнопки в админке при редактировании полей */
    /* ================================================== */
    $('input[name=series_name]').keyup(function() {
        $(this).closest('tr').find('a[name=save_series_btn]').removeClass('disabled');
    });

    $('textarea[name=series_short_desc]').keyup(function() {
        $(this).closest('tr').find('a[name=save_series_btn]').removeClass('disabled');
    });

    $('textarea[name=series_desc]').keyup(function() {
        $(this).closest('tr').find('a[name=save_series_btn]').removeClass('disabled');
    });

    /* Кнопка "Сохранить серию" */
    /* ======================== */
    $('a[name=save_series_btn]').click(function(e) {

        var id = $(this).closest('tr').attr('id');
        var name = $(this).closest('tr').find('input[name=series_name]').val();
        var short_description = $(this).closest('tr').find('textarea[name=series_short_desc]').text();
        var description = $(this).closest('tr').find('textarea[name=series_desc]').text();

        $.ajax({
            type: "post",
            url: "php-scripts/user_functions.php",
            data: "action=update_series&id=" + id + '&name=' + name + '&short_description=' + short_description + '&description=' + description,
            success: function(html){
                if (parseInt(html) == 200) {
                    $(e.target).addClass('disabled');
                    alert('Изменения успешно сохранены!');
                } else {
                    alert(html);
                }
            }
        });

        return false;
    });

    /* Кнопка "Войти в админку" */
    /* ======================== */
    $('button#login_btn').click(function(e) {

        var login = $('input[name=login]').val();
        var pass = $('input[name=password]').val();

        $.ajax({
            type: "post",
            url: "php-scripts/user_functions.php",
            data: "action=login&login=" + login + '&password=' + pass,
            success: function(html){
                if (parseInt(html) == 200) {
                    location.reload(true);
                } else {
                    alert(html);
                }
            }
        });

        return false;
    });

    /* Кнопка "Выйти" */
    /* ============== */
    $('button#logout_btn').click(function(e) {

        $.ajax({
            type: "post",
            url: "php-scripts/user_functions.php",
            data: "action=logout",
            success: function(html){
                location.reload(true);
            }
        });

        return false;
    });
});