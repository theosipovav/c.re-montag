<?php

    require_once('php-scripts/user_functions.php');

    $search_str = (isset($_GET['search'])) ? htmlspecialchars($_GET['search']) : '';
    $page_title = 'РЕ-МОНТАЖ – Результаты поиска';

    require_once('header.php');
    require_once('top_menu.php');
?>

<div class="col-xs-12 col-sm-8 col-md-9">
    <div class="inner_body">

        <?php echo '<h1>Результаты поиска по запросу <i>"'.$search_str.'"</i> !</h1>';

        $search_brand = 0;

        if (mb_stripos($search_str, 'daikin', 0, 'UTF-8') !== false) {
            $search_str = trim(str_replace('daikin', '', strtolower($search_str)));
            $search_brand = 1;
        } elseif (mb_stripos($search_str, 'kentatsu', 0, 'UTF-8') !== false) {
            $search_str = trim(str_replace('kentatsu', '', strtolower($search_str)));
            $search_brand = 2;
        } elseif (mb_stripos($search_str, 'electrolux', 0, 'UTF-8') !== false) {
            $search_str = trim(str_replace('electrolux', '', strtolower($search_str)));
            $search_brand = 3;
        } elseif (mb_stripos($search_str, 'ballu', 0, 'UTF-8') !== false) {
            $search_str = trim(str_replace('ballu', '', strtolower($search_str)));
            $search_brand = 4;
        } elseif (mb_stripos($search_str, 'midea', 0, 'UTF-8') !== false) {
            $search_str = trim(str_replace('midea', '', strtolower($search_str)));
            $search_brand = 5;
        } elseif (mb_stripos($search_str, 'chigo', 0, 'UTF-8') !== false) {
            $search_str = trim(str_replace('chigo', '', strtolower($search_str)));
            $search_brand = 6;
        }

        if ($search_brand == 0) {
            $search_query = $db->query("SELECT * FROM models WHERE name LIKE '%".$search_str."%'");
        } else {
            $search_query = $db->query("SELECT * FROM models WHERE brand_id = '$search_brand' AND name LIKE '%".$search_str."%'");            
        }

        if (mysqli_num_rows($search_query) != 0) {

            $num = 0;
            while ($search_row = $search_query->fetch_assoc()) {

                $num++;

                $brand_query = $db->query("SELECT name FROM brands WHERE id = '$search_row[brand_id]'");
                $brand_row = $brand_query->fetch_assoc();

                $series_query = $db->query("SELECT name FROM series WHERE id = '$search_row[series_id]'");
                $series_row = $series_query->fetch_assoc();

                echo '<p>'.$num.'. <a href="model.php?id='.$search_row['id'].'">'.$brand_row['name'].' '.$search_row['name'].' (серия '.$series_row['name'].')</a></p>';
            }
            echo '</center>';

        } else { ?>

            <p>По Вашему запросу ничего не найдено!</p>

        <?php } ?>
    </div>
</div>

<?php require_once('footer.php'); ?>