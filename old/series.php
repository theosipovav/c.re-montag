<?php

    require_once('php-scripts/user_functions.php');

    $series = (isset($_GET['id'])) ? intval($_GET['id']) : 0;
    $series_query = $db->query("SELECT * FROM series WHERE id = '$series'");
    if (mysqli_num_rows($series_query) != 0) {
        $series_row = $series_query->fetch_assoc();
        $brand = $series_row['brand_id'];
        $page_title = 'РЕ-МОНТАЖ – Серия '.$series_row['name'].' - Подробное описание';
    } else {
        $brand = 0;
        $page_title = 'РЕ-МОНТАЖ – Ошибка. Неверный ID серии';
    }

    require_once('header.php');
    require_once('top_menu.php');
?>

<div class="col-xs-12 col-sm-8 col-md-9">
    <div class="inner_body">

        <?php if ($brand > 0) {

            echo '<h2>СЕРИЯ <b>'.strtoupper($series_row['name']).'</b></h2>';

            $count_img = count_files('images/brands/series/'.$series_row['id']);
            if ($count_img > 1) {
                echo '<ul class="bxslider_series">';
                for ($i = 1; $i <= $count_img; $i++) { 
                    echo '<li><center><img class="series_container" src="images/brands/series/'.$series_row['id'].'/'.$i.'.jpg"></center></li>';
                }
                echo '</ul>';
            } else {
                echo '<center><img class="series_container" src="images/brands/series/'.$series_row['id'].'/1.jpg"></center>';
            }

            echo '<br><br>'.$series_row['description'];
        } else { ?>

            <h1>Внимание!</h1>
            <p>Не указан ID серии кондиционеров!</p>

        <?php } ?>

    </div>
</div>

<?php require_once('footer.php'); ?>