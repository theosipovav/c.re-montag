<?php

    require_once('php-scripts/user_functions.php');

    $brand = (isset($_GET['id'])) ? intval($_GET['id']) : 0;
    $brand_query = $db->query("SELECT * FROM brands WHERE id = '$brand'");
    if (mysqli_num_rows($brand_query) != 0) {
        $brand_row = $brand_query->fetch_assoc();
        $page_title = 'РЕ-МОНТАЖ – Кондиционеры '.$brand_row['name'];
    } else {
        $brand = 0;
        $page_title = 'РЕ-МОНТАЖ – Ошибка. Неверный ID производителя';
    }

    require_once('header.php');
    require_once('top_menu.php');
?>

<div class="col-xs-12 col-sm-8 col-md-9">
    <div class="inner_body">

        <?php if ($brand > 0) {

            echo '<h1>Кондиционеры '.strtoupper($brand_row['name']).'!</h1>

            <div class="brand_image" style="background-image: url(images/brands/brand_'.$brand.'.jpg);"></div>';

            echo $brand_row['description'];

            $series_query = $db->query("SELECT * FROM series WHERE brand_id = '$brand'");
            if (mysqli_num_rows($series_query) != 0) {
                echo '<center>';
                while ($series_row = $series_query->fetch_assoc()) {
                    echo '<a href="#series_'.$series_row['id'].'" class="details_btn">'.$series_row['name'].'</a>';
                }
                echo '</center>';
            }

            $series_query = $db->query("SELECT * FROM series WHERE brand_id = '$brand'");
            if (mysqli_num_rows($series_query) != 0) {
                while ($series_row = $series_query->fetch_assoc()) {
                    echo '<h2 id="series_'.$series_row['id'].'">Серия <b>'.$series_row['name'].'</b></h2>
                         '.$series_row['short_description'].'<center>
                         <a href="series.php?id='.$series_row['id'].'" class="details_btn">Подробнее о серии</a></center>';

                    $count_img = count_files('images/brands/series/'.$series_row['id']);
                    if ($count_img > 1) {
                        echo '<ul class="bxslider_series">';
                        for ($i = 1; $i <= $count_img; $i++) { 
                            echo '<li><center><img class="series_container" src="images/brands/series/'.$series_row['id'].'/'.$i.'.jpg"></center></li>';
                        }
                        echo '</ul>';
                    } else {
                        echo '<center><img class="series_container" src="images/brands/series/'.$series_row['id'].'/1.jpg"></center>';
                    }

                    // On/off
                    $model_query_str = "SELECT * FROM models WHERE series_id = '$series_row[id]' AND type = '1'";
                    $models_query = $db->query($model_query_str);
                    $models_count = mysqli_num_rows($models_query);
                    if ($models_count != 0) {

                        // Desktop
                        echo '<h3>'.strtoupper($series_row['name']).' on/off</h3>';

                        if (mysqli_num_rows($models_query) > 5) {
                            echo '<center><table class="hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['name'].'</td>';
                            }
                            echo '</tr><tr><td>Площадь</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['area'].'</td>';
                            }
                            echo '</tr><tr><td>Цена</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                                echo '<td style="color: #fffe00;">'.$price.'</td>';
                            }
                            echo '</tr><tr class="no_border"><td></td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                            }
                            echo '</tr></tbody></table></center>';

                            echo '<center><table class="hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['name'].'</td>';
                            }
                            echo '</tr><tr><td>Площадь</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['area'].'</td>';
                            }
                            echo '</tr><tr><td>Цена</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                                echo '<td style="color: #fffe00;">'.$price.'</td>';
                            }
                            echo '</tr><tr class="no_border"><td></td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                            }
                            echo '</tr></tbody></table></center>';

                        } else {

                            echo '<center><table class="hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';

                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['name'].'</td>';
                            }
                            echo '</tr><tr><td>Площадь</td>';

                            $models_query = $db->query($model_query_str);
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['area'].'</td>';
                            }
                            echo '</tr><tr><td>Цена</td>';

                            $models_query = $db->query($model_query_str);
                            while ($models_row = $models_query->fetch_assoc()) {
                                $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                                echo '<td style="color: #fffe00;">'.$price.'</td>';
                            }

                            echo '</tr><tr class="no_border"><td></td>';

                            $models_query = $db->query($model_query_str);
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                            }

                            echo '</tr></tbody></table></center>';
                        }

                        // Mobile
                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                            echo '<center><table class="visible-xs visible-sm models"><tbody><tr><td>Модель</td>
                                  <td>'.$models_row['name'].'</td></tr><tr><td>Площадь</td>
                                  <td>'.$models_row['area'].'</td></tr><tr><td>Цена</td>
                                  <td style="color: #fffe00;">'.$price.'</td>
                                  </tr><tr class="no_border">
                                  <td colspan="2"><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>
                                  </tr></tbody></table></center>';
                        }
                    }

                    // Inverter type
                    $model_query_str = "SELECT * FROM models WHERE series_id = '$series_row[id]' AND type = '0'";
                    $models_query = $db->query($model_query_str);
                    $models_count = mysqli_num_rows($models_query);
                    if ($models_count != 0) {

                        // Desktop
                        echo '<h3>'.strtoupper($series_row['name']).' Inverter</h3>';

                        if (mysqli_num_rows($models_query) > 5) {
                            echo '<center><table class="hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['name'].'</td>';
                            }
                            echo '</tr><tr><td>Площадь</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['area'].'</td>';
                            }
                            echo '</tr><tr><td>Цена</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                                echo '<td style="color: #fffe00;">'.$price.'</td>';
                            }
                            echo '</tr><tr class="no_border"><td></td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                            }
                            echo '</tr></tbody></table></center>';

                            echo '<center><table class="hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['name'].'</td>';
                            }
                            echo '</tr><tr><td>Площадь</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['area'].'</td>';
                            }
                            echo '</tr><tr><td>Цена</td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                                echo '<td style="color: #fffe00;">'.$price.'</td>';
                            }
                            echo '</tr><tr class="no_border"><td></td>';
                            $models_query = $db->query("SELECT * FROM models WHERE series_id='$series_row[id]' AND type='1' LIMIT 3, 5");
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                            }
                            echo '</tr></tbody></table></center>';

                        } else {

                            echo '<center><table class="hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';

                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['name'].'</td>';
                            }
                            echo '</tr><tr><td>Площадь</td>';

                            $models_query = $db->query($model_query_str);
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td>'.$models_row['area'].'</td>';
                            }
                            echo '</tr><tr><td>Цена</td>';

                            $models_query = $db->query($model_query_str);
                            while ($models_row = $models_query->fetch_assoc()) {
                                $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                                echo '<td style="color: #fffe00;">'.$price.'</td>';
                            }

                            echo '</tr><tr class="no_border"><td></td>';

                            $models_query = $db->query($model_query_str);
                            while ($models_row = $models_query->fetch_assoc()) {
                                echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                            }

                            echo '</tr></tbody></table></center>';
                        }

                        // Mobile
                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                            echo '<center><table class="visible-xs visible-sm models"><tbody><tr><td>Модель</td>
                                  <td>'.$models_row['name'].'</td></tr><tr><td>Площадь</td>
                                  <td>'.$models_row['area'].'</td></tr><tr><td>Цена</td>
                                  <td style="color: #fffe00;">'.$price.'</td>
                                  </tr><tr class="no_border">
                                  <td colspan="2"><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>
                                  </tr></tbody></table></center>';
                        }
                    }

                    // Inverter type White
                    $model_query_str = "SELECT * FROM models WHERE series_id = '$series_row[id]' AND type = '2'";
                    $models_query = $db->query($model_query_str);
                    if (mysqli_num_rows($models_query) != 0) {

                        // Desktop
                        echo '<h3>'.strtoupper($series_row['name']).' Inverter (белый)</h3>';
                        echo '<center><table class=" hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';
                        
                        while ($models_row = $models_query->fetch_assoc()) {
                            echo '<td>'.$models_row['name'].'</td>';
                        }
                        echo '</tr><tr><td>Площадь</td>';

                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            echo '<td>'.$models_row['area'].'</td>';
                        }
                        echo '</tr><tr><td>Цена</td>';

                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                            echo '<td style="color: #fffe00;">'.$price.'</td>';
                        }

                        echo '</tr><tr class="no_border"><td></td>';

                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                        }

                        echo '</tr></tbody></table></center>';

                        // Mobile
                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                            echo '<center><table class="visible-xs visible-sm models"><tbody><tr><td>Модель</td>
                                  <td>'.$models_row['name'].'</td></tr><tr><td>Площадь</td>
                                  <td>'.$models_row['area'].'</td></tr><tr><td>Цена</td>
                                  <td style="color: #fffe00;">'.$price.'</td>
                                  </tr><tr class="no_border">
                                  <td colspan="2"><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>
                                  </tr></tbody></table></center>';
                        }
                    }

                    // Inverter type Silver
                    $model_query_str = "SELECT * FROM models WHERE series_id = '$series_row[id]' AND type = '3'";
                    $models_query = $db->query($model_query_str);
                    if (mysqli_num_rows($models_query) != 0) {

                        // Desktop
                        echo '<h3>'.strtoupper($series_row['name']).' Inverter (серебро)</h3>';
                        echo '<center><table class=" hidden-xs hidden-sm models"><tbody><tr><td>Модель</td>';
                        
                        while ($models_row = $models_query->fetch_assoc()) {
                            echo '<td>'.$models_row['name'].'</td>';
                        }
                        echo '</tr><tr><td>Площадь</td>';

                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            echo '<td>'.$models_row['area'].'</td>';
                        }
                        echo '</tr><tr><td>Цена</td>';

                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                            echo '<td style="color: #fffe00;">'.$price.'</td>';
                        }

                        echo '</tr><tr class="no_border"><td></td>';

                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            echo '<td><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>';
                        }

                        echo '</tr></tbody></table></center>';

                        // Mobile
                        $models_query = $db->query($model_query_str);
                        while ($models_row = $models_query->fetch_assoc()) {
                            $price = ($models_row['price'] == 0) ? 'По запросу' : number_format($models_row['price'],0,'',' ').' ₽';
                            echo '<center><table class="visible-xs visible-sm models"><tbody><tr><td>Модель</td>
                                  <td>'.$models_row['name'].'</td></tr><tr><td>Площадь</td>
                                  <td>'.$models_row['area'].'</td></tr><tr><td>Цена</td>
                                  <td style="color: #fffe00;">'.$price.'</td>
                                  </tr><tr class="no_border">
                                  <td colspan="2"><a href="model.php?id='.$models_row['id'].'" class="details_btn">Подробнее</a></td>
                                  </tr></tbody></table></center>';
                        }
                    }
                }
            }
        } else { ?>

            <h1>Внимание!</h1>
            <p>Не указан ID производителя кондиционеров!</p>

        <?php } ?>
    </div>
</div>

<?php require_once('footer.php'); ?>