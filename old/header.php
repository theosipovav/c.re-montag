<!DOCTYPE html>
<html class=" " lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page_title; ?></title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css?v=0.07">
    <link rel="stylesheet" href="bxslider/jquery.bxslider.css"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700&display=swap&subset=cyrillic" rel="stylesheet">

    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KL54LW8');</script>
    <!-- End Google Tag Manager -->
    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KL54LW8"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

           ym(54045010, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
           });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/54045010" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    <!-- ============ -->
    <!-- Форма звонка -->
    <!-- ============ -->
    <div id="callbackModal" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>
                            <h4 style="text-align: center;">ОБРАТНЫЙ ЗВОНОК</h4><br>
                            <form class="form-horizontal" action="mail.php" method="post" onsubmit="ym(54045010, 'reachGoal', 'callback'); return true;">
                                <input name="type" type="text" value="callback" hidden>
                                <div class="form-group">
                                    <label for="inputName" class="col-xs-3 control-label" style="padding-right:0;">Имя:</label>
                                    <div class="col-xs-9">
                                        <input name="name" type="text" class="form-control" id="inputName" placeholder="Ваше имя" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPhone" class="col-xs-3 control-label" style="padding-right:0;">Телефон:</label>
                                    <div class="col-xs-9">
                                        <input name="phone" type="text" class="form-control" id="inputPhone" placeholder="Ваш телефон" required>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0;">
                                    <div class="col-xs-12">
                                        <center><button type="submit" class="btn btn-standart">Перезвоните мне</button></center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============ -->
    <!-- Форма письма -->
    <!-- ============ -->
    <div id="emailModal" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>
                            <h4 style="text-align: center;">НАПИШИТЕ НАМ</h4><br>
                            <form class="form-horizontal" action="mail.php" method="post" onsubmit="ym(54045010, 'reachGoal', 'email'); return true;">
                                <input name="type" type="text" value="mail" hidden>
                                <div class="form-group">
                                    <label for="inputName2" class="col-xs-3 control-label" style="padding-right:0;">Имя:</label>
                                    <div class="col-xs-9">
                                        <input name="name" type="text" class="form-control" id="inputName2" placeholder="Ваше имя" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-xs-3 control-label" style="padding-right:0;">E-mail:</label>
                                    <div class="col-xs-9">
                                        <input name="email" type="text" class="form-control" id="inputEmail" placeholder="Ваш e-mail" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputMessage" class="col-xs-3 control-label" style="padding-right:0;">Текст:</label>
                                    <div class="col-xs-9">
                                        <textarea name="message" type="text" class="form-control" id="inputMessage" placeholder="Ваше сообщение" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom: 0;">
                                    <div class="col-xs-12">
                                        <center><button type="submit" class="btn btn-standart">Отправить</button></center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============= -->
    <!-- Форма адресов -->
    <!-- ============= -->
    <div id="adressModal" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>

                            <h4 style="text-align: center;">НАШИ АДРЕСА</h4>

                            <p>г.Пенза, ул.Измайлова, 17а;<br>г.Заречный, ул.Ленина, 36б.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</head>