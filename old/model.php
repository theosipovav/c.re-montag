<?php

require_once('php-scripts/user_functions.php');

$model = (isset($_GET['id'])) ? intval($_GET['id']) : 0;
$model_query = $db->query("SELECT * FROM models WHERE id = '$model'");
if (mysqli_num_rows($model_query) != 0) {
    $model_row = $model_query->fetch_assoc();
    $brand = $model_row['brand_id'];
    $page_title = 'РЕ-МОНТАЖ – Модель '.$model_row['name'].' - Подробное описание';

    $brand_query = $db->query("SELECT name FROM brands WHERE id = '$brand'");
    $brand_row = $brand_query->fetch_assoc();

    $series_id = $model_row['series_id'];
    $series_query = $db->query("SELECT name FROM series WHERE id = '$series_id'");
    $series_row = $series_query->fetch_assoc();

    $price = ($model_row['price'] != 0) ? number_format($model_row['price'], 0, '', ' ').' ₽' : 'Цена по запросу';

} else {
    $brand = 0;
    $page_title = 'РЕ-МОНТАЖ – Ошибка. Неверный ID модели';
}

require_once('header.php');
require_once('top_menu.php');
?>

    <div class="col-xs-12 col-sm-8 col-md-9">
        <div class="inner_body">

            <?php if ($brand > 0) { ?>

                <div class="navlink_container">
                    <a href="brand.php?id=<?=$brand?>" class="nav_link"><?=$brand_row['name']?></a>&nbsp;->
                    <a href="series.php?id=<?=$series_id?>" class="nav_link">Серия <?=$series_row['name']?></a>
                </div>

                <div class="area_details"><span><?=$price?></span><br>Площадь <?=$model_row['area']?></div><br><br>

                <h2>МОДЕЛЬ <b><?=$model_row['name']?></b></h2>

                <?php
                $count_img = count_files('images/brands/series/'.$series_id);
                if ($count_img > 1) {
                    echo '<ul class="bxslider_series">';
                    for ($i = 1; $i <= $count_img; $i++) {
                        echo '<li><center>
                            <img class="series_container" src="images/brands/series/'.$series_id.'/'.$i.'.jpg">
                          </center></li>';
                    }
                    echo '</ul>';
                } else
                    echo '<center><img class="series_container" src="images/brands/series/'.$series_id.'/1.jpg"></center>';
                ?>

                <h3 style="font-size: 20px;">Технические характеристики</h3>
<!--                <center>-->
<!--                    <a href="images/models/--><?//=$model?><!--/details.jpg">-->
<!--                        <img class="model_detail_img" src="images/models/--><?//=$model?><!--/details.jpg">-->
<!--                    </a>-->
<!--                </center>-->

                <table class="table"><tbody>
                    <tr>
                        <td>Серия:</td>
                        <td><?=$series_row['name']?></td>
                    </tr>
                    <tr>
                        <td>Рекомендуемая&nbsp;площадь:</td>
                        <td><?=$model_row['area']?></td>
                    </tr>
                    <tr>
                        <td>Производительность:</td>
                        <td><?=$model_row['perfomance']?></td>
                    </tr>
                    <tr>
                        <td>Уровень шума:</td>
                        <td><?=$model_row['noise']?></td>
                    </tr>
                    <tr>
                        <td>Диапазон рабочих температур, &deg;C</td>
                        <td><?=$model_row['temperature']?></td>
                    </tr>
                    <tr>
                        <td>Особенности:</td>
                        <td><?=$model_row['function']?></td>
                    </tr>
                    <tr>
                        <td>Электропитание:</td>
                        <td><?=$model_row['electropower']?></td>
                    </tr>
                    <tr>
                        <td>Внутренний блок:</td>
                        <td><?=$model_row['inner_block']?> мм.</td>
                    </tr>
                    <tr>
                        <td>Внешний блок:</td>
                        <td><?=$model_row['outer_block']?> мм.</td>
                    </tr>
                    <tr>
                        <td>Страна:</td>
                        <td><?=$model_row['country']?></td>
                    </tr>
                    </tbody>
                </table>
                <p>Остались вопросы? Звоните по номеру <a href="tel:+78412221200">+7 (8412) 22-12-00</p>
            <?php } else { ?>

                <h1>Внимание!</h1>
                <p>Не указан ID модели кондиционера!</p>

            <?php } ?>

        </div>
    </div>

<?php require_once('footer.php'); ?>