<?php

    $recepient = "shop@re-montag.ru, sales@marketing-na100.ru";
    // $recepient = "mobile-sd@yandex.ru";
    $sitename = "РЕ-МОНТАЖ";

    $type = htmlspecialchars(trim($_POST["type"]), ENT_QUOTES, 'UTF-8');

    // Обратный звонок
    if ($type == 'callback') {
        $name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES, 'UTF-8');
        $phone = htmlspecialchars(trim($_POST["phone"]), ENT_QUOTES, 'UTF-8');

        $pagetitle = "Заказ обратного звонка с сайта \"$sitename\"";
        $message = "Имя: $name \nТелефон: $phone";
        mail($recepient, $pagetitle, $message, "Content-type: text/plain; charset=\"utf-8\"\n From: $recepient");
    }

    // Сообщение от клиента
    elseif ($type == 'mail') {
        $name = htmlspecialchars(trim($_POST["name"]), ENT_QUOTES, 'UTF-8');
        $email = htmlspecialchars(trim($_POST["email"]), ENT_QUOTES, 'UTF-8');
        $mess = htmlspecialchars(trim($_POST["message"]), ENT_QUOTES, 'UTF-8');

        $pagetitle = "Сообщение от клиента с сайта \"$sitename\"";
        $message = "Имя: $name \nE-mail: $email \nСообщение: $mess";
        mail($recepient, $pagetitle, $message, "Content-type: text/plain; charset=\"utf-8\"\n From: $recepient");
    }

    header("location: ".$_SERVER['HTTP_REFERER']);
?>