<?php

require_once ('connect.php');

// ================================
// Кол-во файлов в папке
// ================================
function count_files($dir) {
    $c = 0;
    $d = dir($dir);
    while ($str=$d->read()) { 
        if ($str{0}!='.'){ 
            if (is_dir($dir.'/'.$str)) $c += count_files($dir.'/'.$str); 
            else $c++; 
        }; 
    } 
    $d->close();
    return $c; 
}

// ================================
// Проверка авторизации
// ================================
function isAdmin() {
    if (!isset($_SESSION['user_status']) or empty($_SESSION['user_status']) or $_SESSION['user_status'] != 'admin') {
        return false;
    } else {
        return true;
    }
}

// ================================
// Изменить модель в админке
// ================================
if (isset($_POST['action']) and $_POST['action'] === 'update_model') {

    if (isAdmin()) {
        if (!isset($_POST['id']) or empty($_POST['id']) or !isset($_POST['name']) or empty($_POST['name']) or
            !isset($_POST['area']) or empty($_POST['area']) or !isset($_POST['price']) or empty($_POST['price']) or
            !isset($_POST['recommended'])) {

            exit ('Отсутствуют обязательные параметры!');
        }

        $id = intval($db->real_escape_string($_POST['id']));
        $name = $db->real_escape_string($_POST['name']);
        $area = $db->real_escape_string($_POST['area']);
        $price = $db->real_escape_string($_POST['price']);
        $recommended = intval($db->real_escape_string($_POST['recommended']));

        $model_query = $db->query("UPDATE models SET name = '$name', area = '$area', price = '$price', recommended = '$recommended' 
                                   WHERE id = '$id'");
        if ($model_query == true) {
            exit ('200');
        } else {
            exit ('Произошла ошибка при обновлении параметров модели. Пожалуйста, попробуйте еще раз.');
        }
    } else {
        exit ('Вы не авторизованы в системе или Ваша сессия устарела. Пожалуйста, повторите вход в панель управления!');        
    }
}

// ================================
// Изменить брэнд в админке
// ================================
elseif (isset($_POST['action']) and $_POST['action'] === 'update_brand') {

    if (isAdmin()) {
        if (!isset($_POST['id']) or empty($_POST['id']) or !isset($_POST['name']) or empty($_POST['name']) or
            !isset($_POST['description']) or empty($_POST['description'])) {

            exit ('Отсутствуют обязательные параметры!');
        }

        $id = intval($db->real_escape_string($_POST['id']));
        $name = $db->real_escape_string($_POST['name']);
        $description = $db->real_escape_string($_POST['description']);

        $brand_query = $db->query("UPDATE brands SET name = '$name', description = '$description' WHERE id = '$id'");
        if ($brand_query == true) {
            exit ('200');
        } else {
            exit ('Произошла ошибка при обновлении параметров брэнда. Пожалуйста, попробуйте еще раз.');
        }
    } else {
        exit ('Вы не авторизованы в системе или Ваша сессия устарела. Пожалуйста, повторите вход в панель управления!');        
    }
}

// ================================
// Изменить серию в админке
// ================================
elseif (isset($_POST['action']) and $_POST['action'] === 'update_series') {

    if (isAdmin()) {
        if (!isset($_POST['id']) or empty($_POST['id']) or !isset($_POST['name']) or empty($_POST['name']) or
            !isset($_POST['short_description']) or empty($_POST['short_description']) or
            !isset($_POST['description']) or empty($_POST['description'])) {

            exit ('Отсутствуют обязательные параметры!');
        }

        $id = intval($db->real_escape_string($_POST['id']));
        $name = $db->real_escape_string($_POST['name']);
        $short_description = $db->real_escape_string($_POST['short_description']);
        $description = $db->real_escape_string($_POST['description']);

        $series_query = $db->query("UPDATE series SET name = '$name', short_description = '$short_description', description = '$description' WHERE id = '$id'");
        if ($series_query == true) {
            exit ('200');
        } else {
            exit ('Произошла ошибка при обновлении параметров серии. Пожалуйста, попробуйте еще раз.');
        }
    } else {
        exit ('Вы не авторизованы в системе или Ваша сессия устарела. Пожалуйста, повторите вход в панель управления!');        
    }
}

// ================================
// Авторизация в админке
// ================================
elseif (isset($_POST['action']) and $_POST['action'] === 'login') {

    if (isset($_POST['login']) and !empty($_POST['login']) and isset($_POST['password']) and !empty($_POST['password'])) {

        $login = $db->real_escape_string($_POST['login']);
        $password = $db->real_escape_string($_POST['password']);

        $login_query = $db->query("SELECT password FROM users WHERE login = '$login'");
        if (mysqli_num_rows($login_query) != 0) {

            $login_row = $login_query->fetch_assoc();
            $hashed_password = crypt($password, 'must_hard_salt');
            if ($login_row['password'] == $hashed_password) {
                $_SESSION['user_status'] = 'admin';
                exit ('200');
            }
        }
    }

    exit ('Неверный логин и/или пароль!');
}

// ================================
// Выход
// ================================
elseif (isset($_POST['action']) and $_POST['action'] === 'logout') {
    $_SESSION = array();
    session_destroy();
}

?>