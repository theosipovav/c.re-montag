<?php

require_once ('connect.php');

// ================================
// Кол-во файлов в папке
// ================================
function count_files($dir) {
    $c = 0;
    $d = dir($dir);
    while ($str=$d->read()) { 
        if ($str{0}!='.'){ 
            if (is_dir($dir.'/'.$str)) $c += count_files($dir.'/'.$str); 
            else $c++; 
        }; 
    } 
    $d->close();
    return $c; 
}

// ================================
// Форматирует дату
// ================================
// function getRusDate($mysqldate) {
//     $time = strtotime($mysqldate);
//     $month_name = array(1 => '01',
//                         2 => '02',
//                         3 => '03',
//                         4 => '04',
//                         5 => '05',
//                         6 => '06',
//                         7 => '07',
//                         8 => '08',
//                         9 => '09',
//                         10 => '10',
//                         11 => '11',
//                         12 => '12' );

//     $month = $month_name[ date( 'n',$time ) ];
//     $day   = date( 'j',$time );
//     $year  = date( 'Y',$time );
//     $hour  = date( 'G',$time );
//     $min   = date( 'i',$time );
//     $date = "$day.$month.$year, $hour:$min";
//     return $date;
// }

// ===========================================
// Подсчитывает сколько прошло времени с даты
// ===========================================
// function showDate($date) {
//     $time_remain = new Datetime($date);
//     $stf = 0;
//     $curr_time = new Datetime(gmdate("Y-m-d H:i:s", time()));
//     $diff = $curr_time->format('U') - $time_remain->format('U');
 
//     $seconds = array('секунда', 'секунды', 'секунд');
//     $minutes = array('минута', 'минуты', 'минут');
//     $hours = array('час', 'часа', 'часов');
//     $days = array('день', 'дня', 'дней');
//     // $weeks = array('неделя', 'недели', 'недель');
//     $months = array('месяц', 'месяца', 'месяцев');
//     $years = array('год', 'года', 'лет');
//     $decades = array('десятилетие', 'десятилетия', 'десятилетий');
 
//     $phrase = array($seconds, $minutes, $hours, $days, $months, $years, $decades);
//     $length = array(1, 60, 3600, 86400, 2630880, 31570560, 315705600);
//     // $phrase = array($seconds, $minutes, $hours, $days, $weeks, $months, $years, $decades);
//     // $length = array(1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600);
 
//     for ($i = sizeof($length) - 1; ($i >= 0) && (($no = $diff / $length[$i]) <= 1); $i--) ;
//     if ($i < 0) $i = 0;
//     $_time = $curr_time->format('U') - ($diff % $length[$i]);
//     $no = floor($no);
//     $value = sprintf("%d %s ", $no, getPhrase($no, $phrase[$i]));
 
//     if (($stf == 1) && ($i >= 1) && (($curr_time->format('U') - $_time) > 0)) $value .= time_ago($_time);
 
//     return $value . ' назад';
// }

// function getPhrase($number, $titles) {
//     $cases = array (2, 0, 1, 1, 1, 2);
//     return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
// }

// ================================
// Проверка авторизации
// ================================
// function isLogin() {
//     if (!isset($_SESSION['steam_id']) or empty($_SESSION['steam_id'])) {
//         return false;
//     } else {
//         return true;
//     }
// }

// // ================================
// // Проверка авторизации
// // ================================
// function isAdmin() {
//     if (!isset($_SESSION['user_status']) or empty($_SESSION['user_status']) or $_SESSION['user_status'] != '7') {
//         return false;
//     } else {
//         return true;
//     }
// }

// // =================================
// // Получить статистику сервера CSGO
// // =================================
// function getServerStatus($db) {
//     $csgo_online = $db->query("SELECT value FROM steam_data WHERE parameter='csgo_online'")->fetch_assoc()['value'];
//     $csgo_load_level = $db->query("SELECT value FROM steam_data WHERE parameter='csgo_load_level'")->fetch_assoc()['value'];
//     echo '<p>Игроков онлайн: <span style="color: #FFFC00;">'.$csgo_online.';</span><br>Статус Steam API: <span style="color: #A0E69A;">'.$csgo_load_level.'.</span></p>';
// }

// // =======================================
// // Получить id последней записи в истории
// // =======================================
// function getHistoryMaxId($db) {
//     echo $db->query("SELECT value FROM steam_data WHERE parameter='history_max_id'")->fetch_assoc()['value'];
// }

// // ================================
// // Установить новое имя в чате
// // ================================
// if (isset($_POST['action']) and $_POST['action'] === 'set_chat_name') {

//     if (isLogin()) {
//         if (!isset($_POST['chat_name']) or empty($_POST['chat_name'])) {
//             exit ('Ваше имя в чате не может быть пустым!');
//         }

//         $chat_name = $db->real_escape_string($_POST['chat_name']);
        
//         $user_query_res = $db->query("SELECT id FROM steam_users WHERE chat_name='$chat_name'");
//         $user_result = $user_query_res->fetch_assoc();
//         if (!empty($user_result['id'])) {
//             exit ('Это имя в чате уже занято другим пользователем!');
//         }

//         $user_query = $db->query("UPDATE steam_users SET chat_name = '$chat_name' WHERE steam_id = '$_SESSION[steam_id]'");
//         if ($user_query == true) {
//             $_SESSION['chat_name'] = $chat_name;
//             exit ('200');
//         } else {
//             exit ('Произошла ошибка при установке нового имени в чате. Пожалуйста, попробуйте еще раз.');
//         }
//     }
// }

// // ====================================
// // Отправить сообщение в техподдержку
// // ====================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'send_support_message') {

//     if (isLogin()) {
//         if (!isset($_POST['type_issue']) or empty($_POST['type_issue'])) {
//             exit ('Необходимо выбрать тип проблемы!');
//         }

//         if (!isset($_POST['message']) or empty($_POST['message'])) {
//             exit ('Подробно опишите суть проблемы, а также действия, которые Вы предпринимали для ее решения. Это поле не может быть пустым!');
//         }

//         if (!isset($_POST['email']) or empty($_POST['email'])) {
//             exit ('Укажите e-mail, по которому с Вами сможет связаться служба технической поддержки. Это поле не может быть пустым!');
//         }

//         if (!isset($_POST['code']) or empty($_POST['code'])) {
//             exit ('Подтвердите что Вы не робот, введите код с картинки. Если код сложно распознать - нажмите кнопку "Обновить изображение"!');
//         }

//         $issue = $db->real_escape_string(strip_tags($_POST['type_issue']));
//         $message = $db->real_escape_string(strip_tags($_POST['message']));
//         $email = $db->real_escape_string(strip_tags($_POST['email']));
//         $code = strtolower($db->real_escape_string(strip_tags($_POST['code'])));

//         if ($_SESSION['captcha_code'] != $code) {
//             exit('503');
//         }

//         // Добавляем запись в БД
//         // ---------------------
//         $support_result = $db->query("INSERT INTO steam_support_messages (user_id, steam_id, title, message, email, date_open) 
//             VALUES('".$_SESSION['user_id']."', '".$_SESSION['steam_id']."', '$issue', '$message', '$email', UTC_TIMESTAMP())");
        
//         if ($support_result !== true) {
//             exit('Произошла ошибка во время регистрации Вашего сообщения в базе данных. Пожалуйста, попробуйте еще раз.');
//         }

//         // Отправляем сообщение на почту админа
//         // ------------------------------------
//         $headers  = 'MIME-Version: 1.0' . "\r\n";
//         $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//         $headers .= 'From: playpaycsgo.ru <playpay-csgo@yandex.ru>' . "\r\n";

//         $recepient = "mobile-sd@yandex.ru, playpay-csgo@yandex.ru";
//         $pagetitle = "Новое обращение в службу технической поддержки с сайта playpaycsgo.ru";
//         $email_message = 'Имя в Steam: '.$_SESSION['steam_name'].';<br>Steam ID: '.$_SESSION['steam_id'].';<br>Имя в чате: '.$_SESSION['chat_name'].'<br>Баланс: '.$_SESSION['balance'].'<br>E-mail: '.$email.'<br><br><b>Тема сообщения: </b>'.$issue.'<br><b>Текст сообщения: </b>'.$message;

//         mail($recepient, $pagetitle, $email_message, $headers);

//         exit('200');
//     }
// }

// // ====================================
// // Отправить заявку на вывод WebMoney
// // ====================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'remove_money_webmoney') {

//     if (isLogin()) {
//         if (!isset($_POST['wmr_wallet']) or empty($_POST['wmr_wallet'])) {
//             exit ('Не указан или некорректно указан WMR-кошелек для вывода!');
//         }

//         if (!isset($_POST['price_remove']) or empty($_POST['price_remove']) or
//            (floatval($_POST['price_remove']) > $_SESSION['balance'])) {
            
//             exit ('Недостаточно средств на лицевом счете для вывода запрошенной суммы!');
//         }

//         if (!isset($_POST['code']) or empty($_POST['code'])) {
//             exit ('Подтвердите что Вы не робот, введите код с картинки. Если код сложно распознать - нажмите кнопку "Обновить изображение"!');
//         }

//         $wmr_wallet = $db->real_escape_string(strip_tags($_POST['wmr_wallet']));
//         $price_remove = floatval($_POST['price_remove']);
//         $price_remove_actuality = round(floatval($_POST['price_remove']) * 0.99, 2);
//         $code = strtolower($db->real_escape_string(strip_tags($_POST['code'])));

//         if ($_SESSION['captcha_code'] != $code) {
//             exit('503');
//         }

//         // Добавляем запись в БД
//         // ---------------------
//         $rem_wm_result = $db->query("INSERT INTO steam_wallet_history (user_id, steam_id, wmr_wallet, price, status, date_open, pay_system) VALUES('".$_SESSION['user_id']."', '".$_SESSION['steam_id']."', '$wmr_wallet', '$price_remove', '0', UTC_TIMESTAMP(), '0')");
        
//         if ($rem_wm_result !== true) {
//             exit('Произошла ошибка во время регистрации Вашей заявки на вывод средств в базе данных. Пожалуйста, попробуйте еще раз.');
//         }

//         // Резервируем средства с баланса пользователя
//         // -------------------------------------------
//         $db->query("UPDATE steam_users SET balance=balance-'".$price_remove."' WHERE id='".$_SESSION['user_id']."'");

//         // Отправляем сообщение на почту админа
//         // ------------------------------------
//         $headers  = 'MIME-Version: 1.0' . "\r\n";
//         $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//         $headers .= 'From: playpaycsgo.ru <playpay-csgo@yandex.ru>' . "\r\n";

//         $recepient = "mobile-sd@yandex.ru, playpay-csgo@yandex.ru";
//         $pagetitle = "Заявка на вывод средств с сайта playpaycsgo.ru";
//         $email_message = 'Имя в Steam: '.$_SESSION['steam_name'].';<br>Steam ID: '.$_SESSION['steam_id'].';<br>Имя в чате: '.$_SESSION['chat_name'].'<br>Баланс до вывода: '.$_SESSION['balance'].'<br><br><b>WMR-кошелек: </b>'.$wmr_wallet.';<br><b>Сумма списания: </b>'.$price_remove.' руб.;<br><b>Сумма к получению: </b>'.$price_remove_actuality.' руб.';

//         mail($recepient, $pagetitle, $email_message, $headers);

//         exit('200');
//     }

//     // Ошибка авторизации
//     // ------------------
//     else {
//         exit('Ошибка авторизации на сайте! Повторно выполните вход в систему!');
//     }
// }

// // ====================================
// // Отправить заявку на вывод QIWI
// // ====================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'remove_money_qiwi') {

//     if (isLogin()) {
//         if (!isset($_POST['qiwi_wallet']) or empty($_POST['qiwi_wallet'])) {
//             exit ('Не указан или некорректно указан QIWI-кошелек для вывода!');
//         }

//         if (!isset($_POST['price_remove']) or empty($_POST['price_remove']) or
//            (floatval($_POST['price_remove']) > $_SESSION['balance'])) {
            
//             exit ('Недостаточно средств на лицевом счете для вывода запрошенной суммы!');
//         }

//         if (!isset($_POST['code']) or empty($_POST['code'])) {
//             exit ('Подтвердите что Вы не робот, введите код с картинки. Если код сложно распознать - нажмите кнопку "Обновить изображение"!');
//         }

//         $qiwi_wallet = $db->real_escape_string(strip_tags($_POST['qiwi_wallet']));
//         $price_remove = floatval($_POST['price_remove']);
//         $price_remove_actuality = round(floatval($_POST['price_remove']) * 0.97, 2);
//         $code = strtolower($db->real_escape_string(strip_tags($_POST['code'])));

//         if ($_SESSION['captcha_code'] != $code) {
//             exit('503');
//         }

//         // Добавляем запись в БД
//         // ---------------------
//         $rem_qiwi_result = $db->query("INSERT INTO steam_wallet_history (user_id, steam_id, wmr_wallet, price, status, date_open, pay_system) VALUES('".$_SESSION['user_id']."', '".$_SESSION['steam_id']."', '$qiwi_wallet', '$price_remove', '30', UTC_TIMESTAMP(), '1')");
        
//         if ($rem_qiwi_result !== true) {
//             exit('Произошла ошибка во время регистрации Вашей заявки на вывод средств в базе данных. Пожалуйста, попробуйте еще раз.');
//         }

//         // Резервируем средства с баланса пользователя
//         // -------------------------------------------
//         $db->query("UPDATE steam_users SET balance=balance-'".$price_remove."' WHERE id='".$_SESSION['user_id']."'");

//         // Отправляем сообщение на почту админа
//         // ------------------------------------
//         $headers  = 'MIME-Version: 1.0' . "\r\n";
//         $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//         $headers .= 'From: playpaycsgo.ru <playpay-csgo@yandex.ru>' . "\r\n";

//         $recepient = "mobile-sd@yandex.ru, playpay-csgo@yandex.ru";
//         $pagetitle = "Заявка на вывод средств с сайта playpaycsgo.ru";
//         $email_message = 'Имя в Steam: '.$_SESSION['steam_name'].';<br>Steam ID: '.$_SESSION['steam_id'].';<br>Имя в чате: '.$_SESSION['chat_name'].'<br>Баланс до вывода: '.$_SESSION['balance'].'<br><br><b>QIWI-кошелек: </b>'.$qiwi_wallet.';<br><b>Сумма списания: </b>'.$price_remove.' руб.;<br><b>Сумма к получению: </b>'.$price_remove_actuality.' руб.';

//         mail($recepient, $pagetitle, $email_message, $headers);

//         exit('200');
//     }

//     // Ошибка авторизации
//     // ------------------
//     else {
//         exit('Ошибка авторизации на сайте! Повторно выполните вход в систему!');
//     }
// }

// // ================================
// // Отправить сообщение в чат
// // ================================
// if (isset($_POST['action']) and $_POST['action'] === 'send_chat_message') {

//     if (isLogin()) {
//         if (!isset($_POST['message']) or empty($_POST['message'])) {
//             exit ('Нельзя отправлять пустые сообщения');
//         }

//         $message = $db->real_escape_string($_POST['message']);
//         if (!SteamChat::sendMessage($db, $message)) {
//             exit('Произошла ошибка при отправке сообщения. Побробуйте повторить попытку');
//         }

//         exit('200');
//     }

//     exit ('Для отправки сообщений в чат необходимо авторизоваться!');
// }

// // =====================================
// // Отправить приватное сообщение в чат
// // =====================================
// if (isset($_POST['action']) and $_POST['action'] === 'send_private_chat_message') {

//     if (isLogin()) {
//         if (!isset($_POST['user_name']) or empty($_POST['user_name'])) {
//             exit ('Не указано имя получателя приватного сообщения!');
//         }

//         if (!isset($_POST['message']) or empty($_POST['message'])) {
//             exit ('Нельзя отправлять пустые сообщения');
//         }

//         $user_name = $db->real_escape_string($_POST['user_name']);
//         $message = $db->real_escape_string($_POST['message']);

//         if (!SteamChat::sendPrivateMessage($db, $user_name, $message)) {
//             exit('Произошла ошибка при отправке приватного сообщения. Побробуйте повторить попытку!');
//         }

//         exit('200');
//     }

//     exit ('Для отправки сообщений в чат необходимо авторизоваться!');
// }

// // ================================
// // Установить ссылку для обмена
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'set_trade_link') {

//     if (isLogin()) {
//         if (!isset($_POST['trade_link']) or empty($_POST['trade_link'])) {
//             exit ('<p style="color: #faa;">Ссылка на обмен не может быть пустой!</p>');
//         }

//         $trade_link = strip_tags($_POST['trade_link']);
//         $steamBot = new SteamBot(null, $db);
//         $steamBot->loginBot();

//         // echo $steamBot->steam->mobileAuth()->steamGuard()->generateSteamGuardCode();

//         if ($steamBot->checkTradeLink($trade_link) == true) {
//             $user_query = $db->query("UPDATE steam_users SET trade_link = '$trade_link' WHERE steam_id = '$_SESSION[steam_id]'");
//             $_SESSION['trade_link'] = $trade_link;
//         } else {
//             $user_query = $db->query("UPDATE steam_users SET trade_link = '' WHERE steam_id = '$_SESSION[steam_id]'");
//             $_SESSION['trade_link'] = '';
//         }

//         echo $steamBot->error_str;
//     }
// }

// // ================================
// // Обновить инвентарь
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'update_inventory') {
//     if (isLogin()) {
//         $steamInventory = new SteamInventory($_STEAMAPI, $db);
//         if ($steamInventory->updateUserInventory($_SESSION['steam_id']) === true) {
//             echo '200';
//         } else {
//             echo $steamInventory->error_str;
//         }
//     }
// }

// // ================================
// // Выставить предмет на продажу
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'sell_inventory') {
//     if (isLogin()) {
//         if ($_SESSION['trade_link'] == '') {
//             exit('Вы не можете выставлять предметы на продажу, пока не установите корректную ссылку на обмен! <a href="inventory.php#settings">Установить</a>');
//         }

//         if (!isset($_POST['asset_id']) or empty($_POST['asset_id'])) { exit ('Не указан ID предмета!'); }
//         if (!isset($_POST['price']) or empty($_POST['price']) or (floatval($_POST['price']) <= 0)) {
//             exit ('Не указана или некорректная цена предмета!');
//         }

//         $asset_id = $db->real_escape_string(strip_tags($_POST['asset_id']));
//         $price = floatval($_POST['price']);

//         $steamInventory = new SteamInventory($_STEAMAPI, $db);
//         if ($steamInventory->sellInventory($_SESSION['steam_id'], $asset_id, $price) === true) {
//             echo '200';
//         } else {
//             echo $steamInventory->error_str;
//         }
//     }
// }

// // ================================
// // Снять предмет с продажи
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'cancel_sell_inventory') {
//     if (isLogin()) {
//         if (!isset($_POST['asset_id']) or empty($_POST['asset_id'])) { exit ('Не указан ID предмета!'); }

//         $asset_id = $db->real_escape_string($_POST['asset_id']);

//         $steamInventory = new SteamInventory($_STEAMAPI, $db);
//         if ($steamInventory->cancelSellInventory($_SESSION['steam_id'], $asset_id) === true) {
//             echo '200';
//         } else {
//             echo $steamInventory->error_str;
//         }
//     }
// }

// // ================================
// // Снять все предметы с продажи
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'cancel_all_inventory') {
//     if (isLogin()) {
//         $steamInventory = new SteamInventory($_STEAMAPI, $db);
//         if ($steamInventory->cancelAllInventory($_SESSION['steam_id']) === true) {
//             echo '200';
//         } else {
//             echo $steamInventory->error_str;
//         }
//     }
// }

// // ================================
// // Получить инвентарь для магазина
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'get_shop_inventory') {

//     $name = $type = $rarity = $quality = $price_from = $price_to = null;

//     if (isset($_POST['name']) and !empty($_POST['name']) and ($_POST['name'] != '')) {
//         $name = $db->real_escape_string($_POST['name']);
//     }

//     if (isset($_POST['type']) and !empty($_POST['type']) and ($_POST['type'] != 'Все типы')) {
//         $type = $db->real_escape_string($_POST['type']);
//     }

//     if (isset($_POST['rarity']) and !empty($_POST['rarity']) and ($_POST['rarity'] != 'Любая')) {
//         $rarity = $db->real_escape_string($_POST['rarity']);
//     }

//     if (isset($_POST['quality']) and !empty($_POST['quality']) and ($_POST['quality'] != 'Любое')) {
//         $quality = $db->real_escape_string($_POST['quality']);
//     }

//     if (isset($_POST['price_from']) and !empty($_POST['price_from']) and ($_POST['price_from'] != '')) {
//         $price_from = floatval($_POST['price_from']);
//     }

//     if (isset($_POST['price_to']) and !empty($_POST['price_to']) and ($_POST['price_to'] != '')) {
//         $price_to = floatval($_POST['price_to']);
//     }

//     if (isset($_POST['start_page']) and !empty($_POST['start_page']) and (intval($_POST['start_page']) > 0)) {
//         $start_page = intval($_POST['start_page']);
//     } else {
//         $start_page = 1;
//     }

//     $lowest_steam = (isset($_POST['lowest_steam']) and ($_POST['lowest_steam'] == 'true')) ? true : false;
//     $stat_trak = (isset($_POST['stat_trak']) and ($_POST['stat_trak'] == 'true')) ? true : false;

//     $steamInventory = new SteamInventory($_STEAMAPI, $db);
//     $steamInventory->getShopInventory($name, $type, $rarity, $quality, $price_from, $price_to, $lowest_steam, $stat_trak, $start_page);
// }

// // =====================================
// // Получить историю продаж для магазина
// // =====================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'get_history_inventory') {
//     $steamInventory = new SteamInventory($_STEAMAPI, $db);
//     $steamInventory->getHistoryInventory();
// }

// // =============================================
// // Получить карточку оружия на продаже для чата
// // =============================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'get_weapon_card') {

//     if (!isset($_POST['asset_id']) or empty($_POST['asset_id'])) { exit ('Не указан ID предмета!'); }
//     $asset_id = $db->real_escape_string($_POST['asset_id']);

//     $steamInventory = new SteamInventory($_STEAMAPI, $db);
//     $weapon_card = $steamInventory->getSellWeaponCard($asset_id);
//     exit($weapon_card);
// }

// // =============================================
// // Получить название оружия на продаже для чата
// // =============================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'get_weapon_name') {

//     if (!isset($_POST['asset_id']) or empty($_POST['asset_id'])) { exit ('Не указан ID предмета!'); }
//     $asset_id = $db->real_escape_string($_POST['asset_id']);

//     $steamInventory = new SteamInventory($_STEAMAPI, $db);
//     $weapon_name = $steamInventory->getSellWeaponName($asset_id);
//     exit($weapon_name);
// }

// // ================================
// // Купить предмет в магазине
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'buy_inventory') {
//     if (isLogin()) {
//         if (!isset($_POST['asset_id']) or empty($_POST['asset_id'])) { exit ('Не указан ID предмета!'); }
//         $asset_id = $db->real_escape_string(strip_tags($_POST['asset_id']));

//         if ($_SESSION['trade_link'] == '') {
//             exit('Вы не можете покупать предметы, пока не установите корректную ссылку на обмен! <a href="inventory.php#settings">Установить</a>');
//         }

//         $steamInventory = new SteamInventory($_STEAMAPI, $db);
//         if ($steamInventory->buyInventory($_SESSION['steam_id'], $asset_id) === true) {
//             echo '200';
//         } else {
//             echo $steamInventory->error_str;
//         }
//     } else {
//         exit('Для совершения этой операции необходимо выполнить вход через Steam!');
//     }
// }

// // ================================
// // Вкл. / выкл. торговли
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'stop_trade') {
//     if (isLogin()) {
//         if (!isset($_POST['status'])) { exit ('Не указан статус операции!'); }
        
//         $status = intval($_POST['status']);
        
//         // Выключаем торговлю
//         // ------------------
//         // if ($status) {
//         //     $db->query("UPDATE steam_users SET stop_trade = 1 WHERE id = '$_SESSION[user_id]'");
//         // }

//         // Включаем торговлю
//         // -----------------
//         // else {
//         //     $db->query("UPDATE steam_users SET stop_trade = 0 WHERE id = '$_SESSION[user_id]'");
//         // }

//         $steamInventory = new SteamInventory($_STEAMAPI, $db);
//         if ($steamInventory->stopTrade($_SESSION['steam_id'], $status) === true) {
//             echo '200';
//         } else {
//             echo $steamInventory->error_str;
//         }
//     } else {
//         exit('Для совершения этой операции необходимо выполнить вход через Steam!');
//     }
// }

// // ================================
// // Получить имя пользователя в чате
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'get_chat_name') {
//     if (!isset($_POST['user_id']) or empty($_POST['user_id'])) { exit ('Аноним'); }
//     $user_id = intval($_POST['user_id']);
//     $user_query_res = $db->query("SELECT chat_name FROM steam_users WHERE id='$user_id'");
//     $user_result = $user_query_res->fetch_assoc();

//     exit ($user_result['chat_name']);
// }

// // ================================
// // Выход
// // ================================
// elseif (isset($_POST['action']) and $_POST['action'] === 'logout') {

//     $steam_chat = new SteamChat(null, $db);
//     $steam_chat->sendLogout($_SESSION["chat_name"]);

//     $_SESSION = array();
//     session_destroy();
// }

?>