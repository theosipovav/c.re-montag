<!DOCTYPE html>
<html class=" " lang="ru">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $page_title; ?></title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="bxslider/jquery.bxslider.css"/>

    <!-- ====================== -->
    <!-- Форма обратного звонка -->
    <!-- ====================== -->
    <div id="callbackModal" class="modal fade">
        <div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-body"><div class="container-fluid">
            <div class="row">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>

                <h4 style="text-align: center;">ОБРАТНЫЙ ЗВОНОК</h4><br>

                <form class="form-horizontal" action="mail.php" method="post">
                    <div class="form-group">
                        <label for="inputName" class="col-xs-3 control-label" style="padding-right:0;">Имя:</label>
                        <div class="col-xs-9">
                            <input name="name" type="text" class="form-control" id="inputName" placeholder="Ваше имя" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPhone" class="col-xs-3 control-label" style="padding-right:0;">Телефон:</label>
                        <div class="col-xs-9">
                            <input name="phone" type="text" class="form-control" id="inputPhone" placeholder="Ваш телефон" required>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0;">
                        <div class="col-xs-12">
                            <center><button type="submit" class="btn btn-standart">Перезвоните мне</button></center>
                        </div>
                    </div>
                </form>
            </div>
        </div></div></div></div>
    </div>

    <!-- ============ -->
    <!-- Форма письма -->
    <!-- ============ -->
    <div id="emailModal" class="modal fade">
        <div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-body"><div class="container-fluid">
            <div class="row">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>

                <h4 style="text-align: center;">НАПИШИТЕ НАМ</h4><br>

                <form class="form-horizontal" action="mail.php" method="post">
                    <div class="form-group">
                        <label for="inputName2" class="col-xs-3 control-label" style="padding-right:0;">Имя:</label>
                        <div class="col-xs-9">
                            <input name="name" type="text" class="form-control" id="inputName2" placeholder="Ваше имя" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail" class="col-xs-3 control-label" style="padding-right:0;">E-mail:</label>
                        <div class="col-xs-9">
                            <input name="email" type="text" class="form-control" id="inputEmail" placeholder="Ваш e-mail" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputMessage" class="col-xs-3 control-label" style="padding-right:0;">Текст:</label>
                        <div class="col-xs-9">
                            <textarea name="message" type="text" class="form-control" id="inputMessage" placeholder="Ваше сообщение" required></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0;">
                        <div class="col-xs-12">
                            <center><button type="submit" class="btn btn-standart">Отправить</button></center>
                        </div>
                    </div>
                </form>
            </div>
        </div></div></div></div>
    </div>

    <!-- ============= -->
    <!-- Форма адресов -->
    <!-- ============= -->
    <div id="adressModal" class="modal fade">
        <div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-body"><div class="container-fluid">
            <div class="row">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -10px;">×</button>

                <h4 style="text-align: center;">НАШИ АДРЕСА</h4>

                <p>г.Пенза, ул.Измайлова, 17а;<br>г.Заречный, ул.Ленина, 36б.</p>
            </div>
        </div></div></div></div>
    </div>
</head>